<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
  //  return view('welcome');
//});
// form show
Route::get('/', 'HomeController@index');
Route::get('/login', 'HomeController@login');
Route::get('/member', 'HomeController@member');
Route::get('/organization', 'HomeController@organization');
Route::get('/branch', 'HomeController@branch');
Route::get('/areacsetup', 'HomeController@area');
Route::get('/region', 'HomeController@region');
Route::get('/division', 'HomeController@division');
Route::get('/designation', 'HomeController@designation');
Route::get('/project', 'HomeController@project');
Route::get('/orgtype', 'HomeController@orgtype');
Route::get('/loanproduct', 'HomeController@loanproduct');
Route::get('/loanclassification', 'HomeController@loanclassification');
Route::get('/personal', 'HomeController@personal');
Route::get('/loanproductdefination', 'HomeController@loanproductdefination');
Route::get('/loans', 'HomeController@loans');
// end
//start dataload controller
Route::POST('/memberdataload', 'DataLoadController@membre_data');
Route::POST('/branchdataload', 'DataLoadController@branch_data');
Route::POST('/organizationdataload', 'DataLoadController@organization_data');
Route::POST('/loanclassificationdataload', 'DataLoadController@loanclasification_data');
Route::POST('/orgtypedataload', 'DataLoadController@orgtype_data');
Route::POST('/projectdataload', 'DataLoadController@project_data');
Route::POST('/regiondataload', 'DataLoadController@region_data');
Route::POST('/loanproductdataload', 'DataLoadController@loanproduct_data');
Route::POST('/divisiondataload', 'DataLoadController@division_data');
Route::POST('/designationdataload', 'DataLoadController@designation_data');
Route::POST('/areadataload', 'DataLoadController@area_data');
Route::POST('/personaldataload', 'DataLoadController@personal_data');
Route::POST('/loandataload', 'DataLoadController@loan_disbursment');
Route::POST('/datacollect', 'DataLoadController@dataload');
Route::POST('/productlist', 'DataLoadController@productlist');
Route::POST('/loandisbursment', 'DataLoadController@Disbursment');
// end dataload controller
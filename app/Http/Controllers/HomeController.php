<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
    	return view('testform');
    }
    public function login()
    {
        return view('login');
    }
    public function member()
    {
    	return view('member_admission');
    }
    public function organization()
    {
    	return view('organization');
    }
    public function branch()
    {
        return  view('branch');
    }
    public function area()
    {
        return view('area');
    }
    public function region()
    {
        return view('region');
    }
    public function division()
    {
        return view('division');
    }
    public function designation()
    {
        return view('designation');
    }
    public function project()
    {
        return view('project');
    }
    public function orgtype()
    {
        return view('orgtype');
    }
    public function loanproduct()
    {
        return view('loanproduct');
    }
    public function loanclassification()
    {
        return view('loanclassification');
    }
    public function personal()
    {
        return view('personal');
    }
    public function loanproductdefination()
    {
        return view('loanproductdefination');
    }
    public function loans()
    {
        return view('loans');
    }
}

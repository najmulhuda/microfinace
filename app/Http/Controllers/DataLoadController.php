<?php

namespace App\Http\Controllers;

use view;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Session;
use App\Http\Requests;
class DataLoadController extends Controller
{
    public function membre_data(Request $req)
    {
        $MemberId = $req->get('projectid');
        $OrgNo = $req->get('OrgNo');
        $OrgName = $req->get('OrgName');
        $brcode = $req->get('brcode');
        $MemberName = $req->get('MemberName');
        $MemSexId = $req->get('MemSexId');
        $SavingBalance = $req->get('SavingBalance');
        $CalcIntrAmt = $req->get('CalcIntrAmt');
        $TargerAmtSav = $req->get('TargerAmtSav');
        $ApplicationDate = $req->get('ApplicationDate');
        $CloseReason = $req->get('CloseReason');
        $CloseingDate = $req->get('CloseingDate');
        $FName = $req->get('FName');
        $MName = $req->get('MName');
        $MemberStatus = $req->get('MemberStatus');
        $MembershipDate = $req->get('MembershipDate');
        $ReferenceNo = $req->get('ReferenceNo');
        $SurveyReportNo = $req->get('SurveyReportNo');
        $RefferedBy = $req->get('RefferedBy');
        $Age = $req->get('Age');
        $BkashWalletNo = $req->get('BkashWalletNo');
        $DateOfBirth = $req->get('DateOfBirth');
        $DrivingLicenseNo = $req->get('DrivingLicenseNo');
        $FatherName = $req->get('FatherName');
        $MotherName = $req->get('MotherName');
        $NationalId = $req->get('NationalId');
        $Occupation = $req->get('Occupation');
        $PassportNo = $req->get('PassportNo');
        $SpouseName = $req->get('SpouseName');
        $ContactNo = $req->get('ContactNo');
        $Address = $req->get('Address');
        $AddressTitle = $req->get('AddressTitle');
        $City = $req->get('City');
        $ZipCode = $req->get('ZipCode');
        $NomineeName = $req->get('NomineeName');
        $Relationship = $req->get('Relationship');
        $br =explode("-", $brcode);
        $brcd = $br[0];
        $pid =explode("-", $MemberId);
        $proid = $pid[0];

        $data = DB::table('memberlist')->insert(['groupno' =>$OrgNo,'branchid'=>$brcd,'memberid'=>$proid,'membername' =>$MemberName,
            'memsexid' =>$MemSexId,'savbalance'=>$SavingBalance,'calcintramt'=>$CalcIntrAmt,'targetamtsav'=>$TargerAmtSav,'applicationdate'=>$ApplicationDate,
            'closereason'=>$CloseReason,'closingdate'=>$CloseingDate,'memberstatus'=>$MemberStatus,'membershipdate' =>$MembershipDate,
            'reffernceno' =>$ReferenceNo,'refferedby'=>$RefferedBy,'age'=>$Age,'bkashwalletno'=>$BkashWalletNo,
            'dateofbirth'=>$DateOfBirth,'drivinglicenseno'=>$DrivingLicenseNo,'fathername'=>$FatherName,'mothername'=>$MotherName,'nationalid'=>$NationalId,'occupation'=>$Occupation,
            'passportno'=>$PassportNo,'spousename'=>$SpouseName,'contactno'=>$ContactNo,'address' =>$Address,'city' =>$City,
            'zipcode' =>$ZipCode,'nomineename'=>$NomineeName,'relationship'=>$Relationship]);
        if(empty($data))
        {
            //return View('orgtype')->with('Error','Not inserted');
            echo "Data loss";
        }
        else
        {
           return redirect()->back()->with('success','Success!! Member Insert Successfull!!');
        }

    }
    public function branch_data(Request $req)
    {
        $Address2 ="";
        $BranchCode='';
        $aid='';
        $rid='';
        $did='';
    	//$BranchCode = $req->get('BranchCode');
    	$BranchName = $req->get('BranchName');
    	$BranchAddress = $req->get('BranchAddress');
    	$Address1 = $req->get('Address1');
    	$Address2 = $req->get('Address2');
    	$PostCode = $req->get('PostCode');
    	$Longitude = $req->get('Longitude');
    	$Latitude = $req->get('Latitude');
    	$AreaCode = $req->get('AreaCode');
    	$RegionCode = $req->get('RegionCode');
    	$DivisionCode = $req->get('DivisionCode');
        $explode = explode("-",$AreaCode);
        $aid = $explode[0];
        $explode1 = explode("-",$RegionCode);
        $rid = $explode1[0];
        $explode2 = explode("-",$DivisionCode);
        $did = $explode2[0];
    	//$data = DB::select( DB::raw(""));
    	$data = DB::table('branch')->insert(['branchid' =>$BranchCode,'branchname'=>$BranchName,'address1' =>$BranchAddress,
            'address2' =>$Address1,'address3'=>$Address2,'postcode'=>$PostCode,'longi'=>$Longitude,'lati'=>$Latitude,
            'areaid'=>$aid,'regionid'=>$rid,'divisionid'=>$did]);
    	if(empty($data))
        {
            //return View('orgtype')->with('Error','Not inserted');
            echo "Data loss";
        }
        else
        {
          return redirect()->back()->with('success','Success!! Branch Insert Successfull!!');
        }
    }
    public function designation_data(Request $req)
    {
        //$DesignationId =  $req->get("DesignationId");
        $DesignationTitle =  $req->get("DesignationTitle");
        $data = DB::table('designations')->insert(['title'=>$DesignationTitle]);
        if(empty($data))
        {
            //return View('orgtype')->with('Error','Not inserted');
            echo "Data loss";
        }
        else
        {
           return redirect()->back()->with('success','Success!! Designation Insert Successfull!!');
        }
    }
    public function division_data(Request $req)
    {
        $DivisionName =  $req->get("DivisionName");
        $data = DB::table('divisions')->insert(['divisionname'=>$DivisionName]);
        if(empty($data))
        {
            //return View('orgtype')->with('Error','Not inserted');
            echo "Data loss";
        }
        else
        {
          return redirect()->back()->with('success','Success!! Division Insert Successfull!!');
        }
    }
    public function region_data(Request $req)
    {
        $RegionCode = $req->get("RegionCode");
        $RegionName = $req->get("RegionName");
        $DivisionCode = $req->get("DivisionCode");
        $explode = explode("-",$DivisionCode);
        $divid = $explode[0];
        $data = DB::table('region')->insert(['regionname'=>$RegionName,'divistioncode'=>$divid]);
        if(empty($data))
        {
            //return View('region')->with('Error','Not inserted');
            echo "Data loss!";
        }
        else
        {
           return redirect()->back()->with('success','Success!! Region Insert Successfull!!');
        }
    }
    public function area_data(Request $req)
    {
        $AreaCode = $req->get("AreaCode");
        $AreaName = $req->get("AreaName");
        $RegionCode = $req->get("RegionCode");
        $explode = explode("-",$RegionCode);
        $rid = $explode[0];
        $data = DB::table('area')->insert(['areaname'=>$AreaName,'regioncode'=>$rid]);
        if(empty($data))
        {
            //return View('region')->with('Error','Not inserted');
            echo "Data loss!";
        }
        else
        {
            return redirect()->back()->with('success','Success!! Area Insert Successfull!!');
        }
    }
    public function loanclasification_data(Request $req)
    {
        $ProductNo = $req->get("ProductNo");
        $ClassificationName = $req->get("ClassificationName");
        $Duration = $req->get("Duration");
        $StepNo = $req->get("StepNo");
        $data = DB::select( DB::raw("insert into loanclassification(productno, classificationname, duration, stepno) VALUES('$ProductNo','$ClassificationName','$Duration','$StepNo')"));
        /*$data = DB::table('loanclassification')->insert(['productno'=>$ProductNo,'classificationname'=>$ClassificationName,'duration'=>$Duration,'stepno'=>$StepNo]);*/
        if(empty($data))
        {
           // return View('loanclasification')->with('Error','Not inserted');
            echo "data loss";
        }
        else
        {
            //return View('loanclasification')->with('Success','Data insert successfull!!');
            echo "insert  successfull!!!!!";
        }
    }
    public function loanproduct_data(Request $req)
    {
        $Productname = $req->get("Productname");
        $ProductSymbol = $req->get("ProductSymbol");
        $Interest = $req->get("Interest");
        $maxamount = $req->get("maxamount");
        $lenthinmonth = $req->get("lenthinmonth");
        $graceperiod = $req->get("graceperiod");
        $clcoption = $req->get("clcoption");
        $loantype = $req->get("loantype");

        $projectid = $req->get("projectid");
        $insurencepremium = $req->get("insurence");

        $explode = explode("-",$clcoption);
        $clc = $explode[0];
        $explode1 = explode("-",$loantype);
        $lonty = $explode1[0];
        $explode2 = explode("-",$projectid);
        $pid = $explode2[0];

        $data = DB::table('loanproducts')->insert(['productname' =>$Productname,'productsymbole'=>$ProductSymbol,'interestrate'=>$Interest,'maxamount'=>$maxamount,'lengthinmonth' =>$lenthinmonth,'graceperiod'=>$graceperiod,'collectionoption'=>$clc,'loantype'=>$lonty,'projectid'=>$pid,'insurencepremium'=>$insurencepremium]);
        if(empty($data))
        {
            //return View('orgtype')->with('Error','Not inserted');
            echo "Data loss";
        }
        else
        {
           return redirect()->back()->with('success','Success!! Loan Product Insert Successfull!!');
        }
    }
    public function organization_data(Request $req)
    {
        $OrgNo = $req->get("OrgNo");
        $OrgName = $req->get("OrgName");
        $OrgType = $req->get("OrgType");
        $ProjectCode = $req->get("project");
        $brcode = $req->get("brcode");
        $Longitude = $req->get("Longitude");
        $Latitude = $req->get("Latitude");
        $CollectionOption = $req->get("CollectionOption");
        $TargetDate = $req->get("TargetDate");
        $Status = $req->get("Status");
        $AssignedPO = $req->get("AssignedPO");
        $explode = explode("-",$ProjectCode);
        $p = $explode[0];
        $explode1 = explode("-",$brcode);
        $b = $explode1[0];
        $data = DB::table('groups')->insert(['groupno' =>$OrgNo,'groupname'=>$OrgName,'orgtype'=>$OrgType,'projectid'=>$p,'branchcode'=>$b,'lati'=>$Latitude,'longi'=>$Longitude,'collection_option'=>$CollectionOption,'tragetdate'=>$TargetDate,'orgstatus'=>$Status,'assignedpo'=>$AssignedPO]);
        if(empty($data))
        {
            //return View('organization')->with('Error','Not inserted');
            echo "Data not inserted";
        }
        else
        {
            return redirect()->back()->with('success','Success!! Organization Insert Successfull!!');
        }
    }
    public function orgtype_data(Request $req)
    {
        $OrgCatagoryId = $req->get("OrgCatagoryId");
        $OrgTypeName = $req->get("OrgTypeName");
        $data = DB::table('orgtype')->insert(['orgcatid' =>$OrgCatagoryId,'orgtypename'=>$OrgTypeName]);
        if(empty($data))
        {
            //return View('orgtype')->with('Error','Not inserted');
            echo "Data loss";
        }
        else
        {
           return redirect()->back()->with('success','Success!! Organization Type Insert Successfull!!');
        }

    }
    public function project_data(Request $req)
    {
        //$ProjectCode = $req->get("ProjectCode");
        $ProjectName = $req->get("ProjectName");
        $StartDate = $req->get("StartDate");
        $EndDate = $req->get("EndDate");
        $OrganizationType = $req->get("GroupCategory");
        $group = explode("-", $OrganizationType);
        $grp = $group[0];
        $data = DB::table('project')->insert(['projectname' =>$ProjectName,'startdate'=>$StartDate,'enddate'=>$EndDate,'groupcategory'=>$grp]);
        if(empty($data))
        {
            //return View('project')->with('Error','Not inserted');
            echo "Data loss!";
        }
        else
        {
            return redirect()->back()->with('success','Success!! Project Insert Successfull!!');
        }
    }
    
    public function personal_data(Request $req)
    {
        $PIN = $req->get("PIN");
        $Name = $req->get("Name");
        $Designation = $req->get("Designation");
        $ProjectCode = $req->get("ProjectCode");
        $DeviceId = $req->get("DeviceId");
        $Status = $req->get("Status");
        $explode = explode("-",$Designation);
        $designation = $explode[0];
        $project = explode("-",$ProjectCode);
        $p = $project[0];
        $data = DB::table('personal')->insert(['pin' =>$PIN,'name'=>$Name,'designation'=>$designation,'projectcode'=>$p,'deviceid'=>$DeviceId,'status'=>$Status]);
        if(empty($data))
        {
            //return View('region')->with('Error','Not inserted');
            echo "Data loss!";
        }
        else
        {
           return redirect()->back()->with('success','Success!! Personal Information Insert Successfull!!');
        }
    }
    public function loan_disbursment(Request $req)
    {
        //$req->session()->forget('project');

        $project = $req->get('x');
        Session::put('project', $project);
        return redirect()->back();
        /*$memberid = $req->get("memberid");
        $projectid = $req->get("projectid");
        $amount = $req->get("amount");
        $noofinstall = $req->get("noofinstall");
        $maxinstall = $req->get("maxinstall");
        $Installment = $req->get("Installment");
        $interest = $req->get("interest");
        $loantype = $req->get("loantype");
        $installamount = $req->get("installamount");
        $isntalmentpassed = $req->get("isntalmentpassed");
        $intbeforecredit = $req->get("intbeforecredit");
        $principaldue = $req->get("principaldue");
        $interestdue = $req->get("interestdue");
        $principalrelized = $req->get("principalrelized");
        $interestrealized = $req->get("interestrealized");
        $overdue = $req->get("overdue");
        $factor = $req->get("factor");
        $Insurencepre = $req->get("Insurencepre");
        $p = explode("-",$projectid);
        $pro = $p[0];
        $memberid1 = explode("-",$memberid);
        $mem = $memberid1[0];
        $insttype = explode("-", $Installment);
        $inty = $insttype[0];
        $lntyp =explode("-", $loantype);
        $lntype = $lntyp[0];
        $data = DB::table('loans')->insert(['memid' =>$mem,'projectid'=>$pro,'amount'=>$amount,'noofinstallment'=>$noofinstall,'mexnoofinstallment'=>$maxinstall,'installmenttype'=>$inty,'interestrate' =>$interest,'loantype'=>$lntype,'installmentamount'=>$installamount,'installmentpassed'=>$isntalmentpassed,'interestbeforecredit'=>$intbeforecredit,'principaldue'=>$principaldue,'interestdue'=>$interestdue,'principalrealized' =>$principalrelized,'interestrealized'=>$interestrealized,'overdue'=>$overdue,'factor'=>$factor,'insurencepremium'=>$Insurencepre]);
        if(empty($data))
        {
            //return View('region')->with('Error','Not inserted');
            echo "Data loss!";
        }
        else
        {
           return redirect()->back()->with('success','Success!! loan Disbursment Insert Successfull!!');
        }*/
    }
    public function productlist(Request $req)
    {
        $product = $req->get('y');
        Session::put('product', $product);
        return redirect()->back();
    }
    public function Disbursment(Request $req)
    {
          $project = Session::get('project');
          $productid = Session::get('product');
          $MemberId = $req->get('member');
          $amount = $req->get('amount');
          $interest = $req->get('interest');
          $colectionoption = $req->get('colectionoption');
          $loantype = $req->get('loantype');
          $insurencepremium = $req->get('insurencepremium');

          $data = DB::table('loans')->insert(['memid' =>$MemberId,'projectid'=>$project,'product'=>$productid,'amount'=>$amount,'noofinstallment'=>0,'mexnoofinstallment'=>0,'installmenttype'=>$colectionoption,'interestrate' =>$interest,'loantype'=>$loantype,'installmentamount'=>0,'installmentpassed'=>0,'interestbeforecredit'=>0,'principaldue'=>0,'interestdue'=>0,'principalrealized' =>0,'interestrealized'=>0,'overdue'=>0,'factor'=>0,'insurencepremium'=>$insurencepremium]);
        if(empty($data))
        {
            //return View('region')->with('Error','Not inserted');
            echo "Data loss!";
        }
        else
        {
           return redirect()->back()->with('success','Success!! loan Disbursment Insert Successfull!!');
        }
    }

}

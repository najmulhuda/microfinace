<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<?php if(Session::has('success'))
			{
				?>
			   <div class="success">
				<div class="alert alert-success">
				{{Session::get('success')}}
				</div>
			   </div>
			<?php
			} 
			 ?>
	    	<div class="registers">
				<h3 align="center"><u>Groups Setup</u></h3>
				<form action="organizationdataload" method="POST">
					<section class="member" id="one">
						<label>GroupNo</label><br/>
						<input type="text" size="20" name="OrgNo">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>GroupName</label><br/>
					    <input type="text" name="OrgName">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>OrgType</label>
					    <input type="text" name="OrgType">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Project No</label><br/>
						<input type="text" name="project" list="project">
						<datalist id="project">
						 <?php
						 $project =  DB::table('project')->get();
						 if($project->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($project as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->projectname; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>BranchCode</label><br/>
						<input type="text" name="brcode" list="brcode">
						<datalist id="brcode">
						 <?php
						 $branch =  DB::table('branch')->get();
						 if($branch->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($branch as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->branchname; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Longitude</label><br/>
					    <input type="text" name="Longitude"><br/>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Latitude</label><br/>
					   <input type="text" name="Latitude"><br/>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>CollectionOption</label><br/>
					    <input type="text" name="CollectionOption"><br/>
		    	 	</section>

		    	 	<section class="member" id="two">
	    	 			<label>TargetDate</label><br/>
					    <input type="date" name="TargetDate"><br/>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Status</label><br/>
					    <input type="text" name="Status"><br/>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>AssignedPO</label><br/>
					     <input type="text" name="AssignedPO"><br/>
		    	 	</section>
		    	 	<br><br>
					<div class="orgsubmit">
		    	 		<input type="submit" name="" value="SUBMIT">
		    	 	</div>
				</form>
				
			</div>
        </div>
	</div>
@endsection
<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<div class="registerproduct">
				<h3 align="center"><u>Loan Classification Register</u></h3>
				<form action="loanclassificationdataload" method="POST">
					<div class="control">
					<label>ProductNo</label><br/>
					<input type="text" name="ProductNo" list="ProductNo">
					<datalist id="ProductNo">
					 <?php
					 $loanproduct =  DB::table('loanproduct')->get();
					 if($loanproduct->isEmpty())
					 {
					 	?>
		                 <option value="">
		                <?php
					 }
					 else
					 {
					    foreach ($loanproduct as $row) {
					    	?>
					    	 <option value="<?php echo $row->productno; ?>">
						     
					    	<?php
					    }	

					 }
						  
					  ?>
					</datalist>
					<br/>
					<label>ClassificationName</label><br/>
					<input type="text" name="ClassificationName"><br/>
					<label>Duration</label><br/>
					<input type="text" name="Duration"><br/>
					<label>StepNo</label><br/>
					<input type="text" name="StepNo"><br/>
					<br/>
					<div class="submit">
						<div class="btnsubmit"><button type="submit" class="btn btn-success">SUBMIT</button></div>
					</div>
					
					<br/>
					</div>
				</form>
				
			</div>
        </div>
	</div>
@endsection
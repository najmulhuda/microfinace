<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<div class="test">
				<h1>Login</h1>
			    <form method="post">
			    	<input type="text" name="u" placeholder="Username" required="required" />
			        <input type="password" name="p" placeholder="Password" required="required" />
			        <button type="submit" class="btn btn-primary btn-block btn-large">Let me in.</button>
			    </form>
			</div>
        </div>
	</div>
@endsection
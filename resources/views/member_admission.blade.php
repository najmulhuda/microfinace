<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<?php if(Session::has('success'))
			{
				?>
			   <div class="success">
				<div class="alert alert-success">
				{{Session::get('success')}}
				</div>
			   </div>
			<?php
			} 
			 ?>
	    	 <div class="registers">
	    	 	<form action="memberdataload" method="POST">
		    	 	<section class="member" id="two">
	    	 			<label>MemberName</label>
	    	 			<input type="text" name="MemberName">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>OrgNo</label>
	    	 			<input type="text" name="OrgNo" list="OrgNo">
						<datalist id="OrgNo">
						 <?php
						 $groups =  DB::table('groups')->get();
						 if($groups->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($groups as $row) {
						    	?>
						    	 <option value="<?php echo $row->groupno; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>OrgName</label>
	    	 			<input type="text" name="OrgName" list="OrgName">
						<datalist id="OrgName">
						 <?php
						 $groups =  DB::table('groups')->get();
						 if($groups->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($groups as $row) {
						    	?>
						    	 <option value="<?php echo $row->groupname; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<br><br><br>
		    	 	<section class="member" id="one">
						<label>Branch Id</label>
						<input type="text" name="brcode" list="brcode">
						<datalist id="brcode">
						 <?php
						 $branch =  DB::table('branch')->get();
						 if($branch->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($branch as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->branchname; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<section class="member" id="one">
						<label>Project Id</label>
		
						  <input type="text" name="projectid" list="projectid">
						<datalist id="projectid">
						 <?php
						 $project =  DB::table('project')->get();
						 if($project->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($project as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->projectname; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>MemSexId</label>
	    	 			<input type="text" name="MemSexId">
		    	 	</section>
		    	 	<br><br><br>
		    	 	<section class="member" id="two">
	    	 			<label>SavingBalance</label>
	    	 			<input type="text" name="SavingBalance">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>CalcIntrAmt</label>
	    	 			<input type="text" name="CalcIntrAmt">
		    	 	</section>
		    	 	<section class="member" id="one">
						<label>TargerAmtSav</label>
						<input type="text" name="TargerAmtSav">
		    	 	</section>
		    	 	
		    	 	<br><br><br>
		    	 	<section class="member" id="two">
	    	 			<label>ApplicationDate</label>
	    	 			<input type="date" name="ApplicationDate">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>CloseReason</label>
	    	 			<input type="text" name="CloseReason">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>CloseingDate</label>
	    	 			<input type="text" name="CloseingDate">
		    	 	</section>
		    	 	
		    	 	<br><br><br>
		    	 	<section class="member" id="one">
						<label>MemberStatus</label>
						<input type="text" name="MemberStatus">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>MembershipDate</label>
	    	 			<input type="date" name="MembershipDate">
		    	 	</section>
		    	 	<section class="member" id="three">
	    	 			<label>ReferenceNo</label>
	    	 			<input type="text" name="ReferenceNo">
		    	 	</section>
		    	 	<br><br><br>
		    	 	<section class="member" id="two">
	    	 			<label>RefferedBy</label>
	    	 			<input type="text" name="RefferedBy">
		    	 	</section>
		    	 	<section class="member" id="one">
						<label>Age</label><br>
						<input type="text" name="Age">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>BkashWalletNo</label>
	    	 			<input type="text" name="BkashWalletNo">
		    	 	</section>
		    	 	<br><br><br>
		    	 	<section class="member" id="two">
	    	 			<label>DateOfBirth</label>
	    	 			<input type="date" name="DateOfBirth">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>DrivingLicenseNo</label>
	    	 			<input type="text" name="DrivingLicenseNo">
		    	 	</section>
		    	 	<section class="member" id="one">
						<label>FatherName</label>
						<input type="text" name="FatherName">
		    	 	</section>
		    	 	<br><br><br>
		    	 	<section class="member" id="two">
	    	 			<label>MotherName</label>
	    	 			<input type="text" name="MotherName">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>NationalId</label>
	    	 			<input type="text" name="NationalId">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Occupation</label>
	    	 			<input type="text" name="Occupation">
		    	 	</section>
		    	 	<br><br><br>
		    	 	<section class="member" id="one">
						<label>PassportNo</label>
						<input type="text" name="PassportNo">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>SpouseName</label>
	    	 			<input type="text" name="SpouseName">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>ContactNo</label>
	    	 			<input type="text" name="ContactNo">
		    	 	</section>
		    	 	<br><br><br>
		    	 	<section class="member" id="two">
	    	 			<label>Address</label>
	    	 			<input type="text" name="Address">
		    	 	</section>
		    	 	<section class="member" id="one">
						<label>City</label><br>
						<input type="text" name="City">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>ZipCode</label>
	    	 			<input type="text" name="ZipCode">
		    	 	</section>
		    	 	<br><br><br>
		    	 	<section class="member" id="two">
	    	 			<label>NomineeName</label>
	    	 			<input type="text" name="NomineeName">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Relationship</label>
	    	 			<input type="text" name="Relationship">
		    	 	</section>
		    	 	<div class="membersubmit">
		    	 		<input type="submit" name="" value="SUBMIT">
		    	 	</div>
		    	 	
	    	 	</form>
	    	 </div>
        </div>
	</div>
@endsection


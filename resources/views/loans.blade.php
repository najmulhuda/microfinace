<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<?php if(Session::has('success'))
			{
				?>
			   <div class="success">
				<div class="alert alert-success">
				{{Session::get('success')}}
				</div>
			   </div>
			<?php
			} 
			 ?>
	    	<div class="disburs">
				<h3 align="center"><u>Loan Disbursment</u></h3>
				<?php
				$pid=0;
				$pname ='';
				$loantype='';
				 if (Session::has('project')) {
                       $pid = Session::get('project'); 
                       Session::put('pid', $pid);
                   } 
                   $data = DB::table('project')->where('id',$pid)->get();
                   if($data->isEmpty())
                   {

                   }
                   else
                   {
                   	 $pname = $data[0]->projectname;
                   }
				$pid=0;
				$pdcname='';
				$pduct=0;
				$maxamount='';
				$interestrate='';
				$collectionoption='';
				$insurence='';
				 if (Session::has('product')) {
                       $pduct = Session::get('product'); 

                      /* $data2 = DB::select( DB::raw("select productname,maxamount,interestrate,collectionoption,insurencepremium,loantype,installmenttype from loanproducts,installmenttype where productid= $pduct and id= $pduct"));*/
                      $data2 = DB::table('loanproducts')->where('productid',$pduct)->get();
                       if(empty($data2))
                       {

                       }
                       else
                       {
                       	 $pdcname = $data2[0]->productname;
                       	 $maxamount = $data2[0]->maxamount;
                       	 $interestrate = $data2[0]->interestrate;
                       	 $collectionoption = $data2[0]->collectionoption;
                       	 $insurence = $data2[0]->insurencepremium;
                       	 $loantype = $data2[0]->loantype;
                       }
                   }
                       ?>
				<section class="member">
					<label>Project Id</label>
					<form method="post" action="loandataload">
					    <select name="x" id="wgtmsr" onChange="this.form.submit();">
					    	<option value="1"><?php echo $pname; ?></option>
					        <option value="1">DABI</option>
					        <option value="2">progoti</option>
					    </select>
					</form>
		    	</section>
		    	<section class="member">
		    		<label>Products</label>
		    		<form method="post" action="productlist">
						<select name="y" id="wgtmsr" onChange="this.form.submit();">
							<option value="1"><?php echo $pdcname; ?></option>
							<option value="1">Test</option>
							<option value="2">New Test</option>
						</select>
				   </form>
		    	</section>
		    	<form action="loandisbursment" method="POST">
		    		<section class="member">
		    		<?php
		    		if (Session::has('pid')) {
                       $pid = Session::get('pid'); 
                       //Session::put('pid', $pid);
                   }
		    		?>
		    		<label>Member Name</label><br>
					<select name="member" id="wgtmsr">
						<?php
						if($pid !='')
					    { 
							 $data1 = DB::table('memberlist')->where('memberid',$pid)->get();
							 if($data1->isEmpty())
							 {

							 }
							 else{
							 	$membername = $data1[0]->membername;
							 	$rowid = $data1[0]->id;
							 }
							 ?>
							<option value="<?php echo $rowid; ?>"><?php echo $membername; ?></option>
							<?php
								foreach ($data1 as $r) {
									?>
									<option value="<?php echo $r->id; ?>"><?php echo $r->membername; ?></option>
									<?php
								}
						}
						
                       ?>
					
					</select>
		    	</section>
		    		<section class="member">
		    		     <label>Amount</label>
					     <input type="text" name="amount" value="<?php echo $maxamount; ?>">
			    	</section>
			    	<section class="member">
			    		<label>Interest</label>
						<input type="text" name="interest" value="<?php echo $interestrate; ?>">
			    	</section>
			    	<section class="member">
			    		<label>Colection Option</label>
						<input type="text" name="colectionoption" value="<?php echo $collectionoption; ?>">
			    	</section>
			    	<section class="member">
			    		<label>Loan Type</label>
						<input type="text" name="loantype" value="<?php echo $loantype; ?>">
			    	</section>
			    	<section class="member">
			    		<label>Insurence Premium</label>
						<input type="text" name="insurencepremium" value="<?php echo $insurence; ?>"><br>
			    	</section>
			    	<section class="membersub">
			    		<div class="disburssubmit">
				    		<input type="submit" value="SUBMIT">
				    </div>
			    	</section>
			    	
				 </form>
				
			</div>
        </div>
	</div>
@endsection
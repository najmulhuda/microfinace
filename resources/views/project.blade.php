<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<?php if(Session::has('success'))
			{
				?>
			   <div class="success">
				<div class="alert alert-success">
				{{Session::get('success')}}
				</div>
			   </div>
			<?php
			} 
			 ?>
	    	<div class="registerproject">
				<h3 align="center"><u>Project Register</u></h3>
				<form action="projectdataload" method="POST">
					<div class="control">
					<label>ProjectName</label><br/>
					<input type="text" name="ProjectName"><br/>
					<label>StartDate</label><br/>
					<input type="date" name="StartDate"><br/>
					<label>EndDate</label><br/>
					<input type="date" name="EndDate"><br/>
					<label>GroupCategory</label><br/>
					<input type="text" name="GroupCategory" list="GroupCategory">
					<datalist id="GroupCategory">
					 <?php
					 $orgtype =  DB::table('groups')->get();
					 if($orgtype->isEmpty())
					 {
					 	?>
                         <option value="">
                        <?php
					 }
					 else
					 {
					    foreach ($orgtype as $row) {
					    	?>
					    	 <option value="<?php echo $row->groupno."-".$row->groupname; ?>">
						     
					    	<?php
					    }	

					 }
						  
					  ?>
					</datalist>
					<br/>
					<br/>
					<div class="submit">
						<input type="submit" name="" value="SUBMIT">
					</div>
					
					<br/>
					</div>
				</form>
				
			</div>
        </div>
	</div>
@endsection

<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<?php if(Session::has('success'))
			{
				?>
			   <div class="success">
				<div class="alert alert-success">
				{{Session::get('success')}}
				</div>
			   </div>
			<?php
			} 
			 ?>
	    	<div class="registerarea">
				<h3 align="center"><u>Region Register</u></h3>
				<form action="regiondataload" method="POST">
					<div class="control">
					<label>RegionName</label><br/>
					<input type="text" name="RegionName"><br/>
					<label>DivisionCode</label><br/>
					<input type="text" name="DivisionCode" list="DivisionCode">
					<datalist id="DivisionCode">
					 <?php
					 $divisions =  DB::table('divisions')->get();
					 if($divisions->isEmpty())
					 {
					 	?>
                         <option value="">
                        <?php
					 }
					 else
					 {
					    foreach ($divisions as $row) {
					    	?>
					    	 <option value="<?php echo $row->id."-".$row->divisionname; ?>">
						     
					    	<?php
					    }	

					 }
						  
					  ?>
					</datalist>
					<br/>
					<br/>
					<br/>
					<div class="submit">
						<input type="submit" name="" value="SUBMIT">
					</div>
				</div>
				</form>
				
			</div>
        </div>
	</div>
@endsection

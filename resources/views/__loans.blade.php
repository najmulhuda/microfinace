<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
<script type="text/javascript">
	function chk()
	{
		var projectid = document.getElementById('pid').value;
		var datastring = 'name=' +projectid;
		alert(datastring);
		/*$.ajax({
			type:"post",
			url:"datacollect",
			data:datastring,
			cache:false,
			success: function(responseText)
			{
				if(responseText==1)
				{
					alert("hello");
				}
			}

		});*/
	}
</script>
	<div class="right_col" role="main">
	    <div class="container">
	    	<?php if(Session::has('success'))
			{
				?>
			   <div class="success">
				<div class="alert alert-success">
				{{Session::get('success')}}
				</div>
			   </div>
			<?php
			} 
			 ?>
	    	<div class="registers">
				<h3 align="center"><u>Loan Disbursment</u></h3>
				<form action="loandataload" method="POST">
					<section class="member" onchange="myFunction()">
	    	 			<label>Project Id</label>
					    <input type="text" name="projectid" id="pid" list="projectid" >
						<datalist id="projectid">
						 <?php
						 $project =  DB::table('project')->get();
						 if($project->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($project as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->projectname; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<script type="text/javascript">

					$(document).ready(function(){
					    $("#pid").blur(function(){
					        var access = jQuery.trim($("form input[name='projectid']").val());
					        alert(access);
					        ///alert("This input field has lost its focus.");
					        /*$("span.validation").html("");
					            var datastring = 'accession='+ access;
					            //alert(datastring);
					             $.ajax({
					                             type: "POST", // type
					                             url: "check_accession.php", // request file the 'check_email.php'
					                             data: datastring, // post the data
					                               success: function(responseText) { // get the response
					                                  // alert(responseText);
					                                 if(responseText ==1) { // if the response is 1
					                                     $("span.accession_val").html("<img src='images/invalid.png'> Accession No are already exist.");
					                                     $("span.loading").html("");
					                                 } 
					                             } // end success
					                 });*/

					    });
					});
					</script>

		    	 
					<section class="member" id="one">
						<label>Member Id</label>
						<input type="text" name="memberid" list="memberid">
						<datalist id="memberid">
						 <?php
						 $memberlist =  DB::table('memberlist')->get();
						 if($memberlist->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($memberlist as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->membername; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	
		    	 	<section class="member" id="two">
	    	 			<label>Amount</label>
					    <input type="text" name="amount">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>No Of Installment</label>
						<input type="text" name="noofinstall">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Max No Of Installment</label>
						<input type="text" name="maxinstall">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Installment Type</label>
						<input type="text" name="Installment" list="Installment">
						<datalist id="Installment">
						 <?php
						 $installmenttype =  DB::table('installmenttype')->get();
						 if($installmenttype->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($installmenttype as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->installmenttype; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Interest Rate</label>
					    <input type="text" name="interest">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Loan Type</label>
					   <input type="text" name="loantype" list="loantype">
						<datalist id="loantype">
						 <?php
						 $loantype =  DB::table('loantype')->get();
						 if($loantype->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($loantype as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->loantype; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Installment Amount</label>
					    <input type="text" name="installamount">
		    	 	</section>

		    	 	<section class="member" id="two">
	    	 			<label>Installment Passed</label>
					    <input type="text" name="isntalmentpassed">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Interest Before Credit</label>
					    <input type="text" name="intbeforecredit">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Principal Due</label>
					     <input type="text" name="principaldue">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Interest Due</label>
					     <input type="text" name="interestdue">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Principal Realized</label>
					     <input type="text" name="principalrelized">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Interest Realized</label>
					     <input type="text" name="interestrealized">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Over Due</label>
					     <input type="text" name="overdue">
		    	 	</section>

		    	 	<section class="member" id="two">
	    	 			<label>factor</label><br>
					     <input type="text" name="factor">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Insurence Premium</label>
					     <input type="text" name="Insurencepre">
		    	 	</section>
		    	 	<br><br>
					<div class="loansubmit">
		    	 		<input type="submit" name="" value="SUBMIT">
		    	 	</div>
				</form>
				
			</div>
        </div>
	</div>
@endsection
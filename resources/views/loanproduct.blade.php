<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<?php if(Session::has('success'))
			{
				?>
			   <div class="success">
				<div class="alert alert-success">
				{{Session::get('success')}}
				</div>
			   </div>
			<?php
			} 
			 ?>
	    	<div class="registers">
				<h3 align="center"><u>Loan Products Setup</u></h3>
				<form action="loanproductdataload" method="POST">
					<section class="member" id="one">
						<label>Product Name</label>
					<input type="text" size="20" name="Productname">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>ProductSymbol</label>
					    <input type="text" name="ProductSymbol">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Interest Rate</label>
					    <input type="text" name="Interest">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Max Amount</label>
					    <input type="text" name="maxamount">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Length In Month</label>
					    <input type="text" name="lenthinmonth">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Grace Period</label><br/>
					    <input type="text" name="graceperiod"><br/>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Collection Option</label>
					    <input type="text" name="clcoption" list="clcoption">
						<datalist id="clcoption">
						 <?php
						 $installmenttype =  DB::table('installmenttype')->get();
						 if($installmenttype->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($installmenttype as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->installmenttype; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Loan Type</label><br/>
						<input type="text" name="loantype" list="loantype">
						<datalist id="loantype">
						 <?php
						 $loantype =  DB::table('loantype')->get();
						 if($loantype->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($loantype as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->loantype; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>

		    	 	<section class="member" id="two">
	    	 			<label>Project Id</label><br/>
						<input type="text" name="projectid" list="projectid">
						<datalist id="projectid">
						 <?php
						 $project =  DB::table('project')->get();
						 if($project->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($project as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->projectname; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Insurence Premium</label><br/>
						<input type="text" name="insurence">
		    	 	</section>
		    	 	<br><br>
					<div class="orgsubmit">
		    	 		<input type="submit" name="" value="SUBMIT">
		    	 	</div>
				</form>
				
			</div>
        </div>
	</div>
@endsection
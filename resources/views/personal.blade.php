<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<?php if(Session::has('success'))
			{
				?>
			   <div class="success">
				<div class="alert alert-success">
				{{Session::get('success')}}
				</div>
			   </div>
			<?php
			} 
			 ?>
	    	<div class="registers">
				<h3 align="center"><u>Personal Register</u></h3>
				<form action="personaldataload" method="POST">
					<div class="control">
					<label>PIN</label><br/>
					<input type="text" size="20" name="PIN"><br/>
					<label>Name</label><br/>
					<input type="text" name="Name"><br/>
					<label>Designation</label><br/>
					<input type="text" name="Designation" list="Designation">
					<datalist id="Designation">
					 <?php
					 $Designation =  DB::table('designations')->get();
					 if($Designation->isEmpty())
					 {
					 	?>
                         <option value=""></option>
                        <?php
					 }
					 else
					 {
					    foreach ($Designation as $row) {
					    	?>
					    	 <option value="<?php echo $row->id."-".$row->title; ?>"></option>
						     
					    	<?php
					    }	

					 }
						  
					  ?>
					</datalist>
					<br/>
					<label>ProjectCode</label><br/>
					<input type="text" name="ProjectCode" list="ProjectCode">
					<datalist id="ProjectCode">
					 <?php
					 $project =  DB::table('project')->get();
					 if($project->isEmpty())
					 {
					 	?>
                         <option value="">
                        <?php
					 }
					 else
					 {
					    foreach ($project as $row) {
					    	?>
					    	 <option value="<?php echo $row->id."-".$row->projectname; ?>">
						     
					    	<?php
					    }	

					 }
						  
					  ?>
					</datalist>
					<br/>
					<label>DeviceId</label><br/>
					<input type="text" name="DeviceId"><br/>
					<label>Status</label><br/>
					<input type="text" name="Status"><br/>
					
					<br/>
					<div class="submit">
						<input type="submit" name="" value="SUBMIT">
					</div>
					
					<br/>
					</div>
				</form>
				
			</div>
        </div>
	</div>
@endsection
<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<?php if(Session::has('success'))
			{
				?>
			   <div class="success">
				<div class="alert alert-success">
				{{Session::get('success')}}
				</div>
			   </div>
			<?php
			} 
			 ?>
	    	<div class="registers">
				<h3 align="center"><u>Branch Setup</u></h3>
				<form action="branchdataload" method="POST">
					<section class="member" id="one">
						<label>BranchName</label>
					     <input type="text" name="BranchName">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>BranchAddress</label>
					    <input type="text" name="BranchAddress">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Address1</label>
					     <input type="text" name="Address1">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Address2</label>
					     <input type="text" name="Address2">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>PostCode</label>
					    <input type="text" name="PostCode">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Longitude</label>
				     	<input type="text" name="Longitude">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>Latitude</label>
					   <input type="text" name="Latitude">
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>AreaCode</label><br/>
						<input type="text" name="AreaCode" list="AreaCode">
						<datalist id="AreaCode">
						 <?php
						 $area =  DB::table('area')->get();
						 if($area->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($area as $row) {
						    	?>
						    	 <option value="<?php echo $row->id; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>

		    	 	<section class="member" id="two">
	    	 			<label>RegionCode</label><br/>
						<input type="text" name="RegionCode" list="RegionCode">
						<datalist id="RegionCode">
						 <?php
						 $region =  DB::table('region')->get();
						 if($region->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($region as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->regionname; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<section class="member" id="two">
	    	 			<label>DivisionCode</label><br/>
						<input type="text" name="DivisionCode" list="DivisionCode">
						<datalist id="DivisionCode">
						 <?php
						 $divisions =  DB::table('divisions')->get();
						 if($divisions->isEmpty())
						 {
						 	?>
	                         <option value="">
	                        <?php
						 }
						 else
						 {
						    foreach ($divisions as $row) {
						    	?>
						    	 <option value="<?php echo $row->id."-".$row->divisionname; ?>">
							     
						    	<?php
						    }	

						 }
							  
						  ?>
						</datalist>
		    	 	</section>
		    	 	<br><br>
					<div class="orgsubmit">
		    	 		<input type="submit" name="" value="SUBMIT">
		    	 	</div>
				</form>
				
			</div>
        </div>
	</div>
@endsection
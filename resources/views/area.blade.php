<!-- <?php 
$username = Session::get('username');
if($username=='')
{
	
	?>
	<script>
	  window.location.href = 'logout';
	</script>
	
	<?php 
	
}
?> -->

@extends('header')
@section('content')
	<div class="right_col" role="main">
	    <div class="container">
	    	<?php if(Session::has('success'))
			{
				?>
			   <div class="success">
				<div class="alert alert-success">
				{{Session::get('success')}}
				</div>
			   </div>
			<?php
			} 
			 ?>
	    	<div class="registerarea">
				<h3 align="center"><u>Area Register</u></h3>
				<form action="areadataload" method="POST">
					<div class="control">
					<label>AreaName</label><br/>
					<input type="text" name="AreaName"><br/>
					<label>RegionCode</label><br/>
					<input type="text" name="RegionCode" list="RegionCode">
					<datalist id="RegionCode">
					 <?php
					 $area =  DB::table('region')->get();
					 if($area->isEmpty())
					 {
					 	?>
                         <option value="">
                        <?php
					 }
					 else
					 {
					    foreach ($area as $row) {
					    	?>
					    	 <option value="<?php echo $row->id; ?>">
						     
					    	<?php
					    }	

					 }
						  
					  ?>
					</datalist>
					<br />
					<br />
					<br />
					<div class="submit">
						<input type="submit" name="" value="SUBMIT">
					</div>
				</div>
				</form>
				
			</div>
        </div>
	</div>
@endsection
